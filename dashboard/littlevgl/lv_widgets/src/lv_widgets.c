#include <lv_widgets/src/lv_widgets.h>
#include LV_THEME_DEFAULT_INCLUDE

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define SCREEN_WIDTH   480
#define SCREEN_HEIGHT  272
#define TOP_BAR_HEIGHT 20

LV_IMG_DECLARE(light_on);
LV_IMG_DECLARE(light_off);

/*******************************************************************************
 * Variables
 ******************************************************************************/
static lv_obj_t * tab_view;
static lv_obj_t * home_tab;
static lv_obj_t * calendar;
static lv_obj_t * hours_roller;
static lv_obj_t * minutes_roller;
static lv_obj_t * left_turn_signal;
static lv_obj_t * right_turn_signal;
static lv_obj_t * batt_label;
static lv_obj_t * sw_sensor;

static lv_anim_t signal_left;
static lv_anim_t signal_right;

static lv_style_t date_and_time_style;
static lv_style_t bar_style;

static bool isLeftEnable = false;
static bool isRightEnable = false;
static bool isLightOn = false;

extern rtc_datetime_t date;
extern uint8_t colors_176x176[61952];
extern VL53L5CX_Configuration tof_sensor;
extern bool frontSensorConnected;
extern bool backSensorConnected;

lv_obj_t * date_and_time_label;
lv_obj_t * temp_label;

bool tempLabelEnable = false;
bool tofSensorEnable = false;

lv_obj_t * img_from_sensor;

uint8_t speed = 0;

lv_img_dsc_t my_img_from_sensor = {
    .header.always_zero = 0,
    .header.w = 176,
    .header.h = 176,
    .data_size = 30976 * LV_COLOR_DEPTH / 8,
    .header.cf = LV_IMG_CF_TRUE_COLOR,
    .data = colors_176x176,
};

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void lv_widgets(void);

static void create_home_tab(lv_obj_t *);
static void home_create(lv_obj_t *);
static void set_date_and_time(lv_obj_t *, lv_event_t event);
static void check_date(lv_obj_t *, lv_event_t);
static void switch_left_signal(lv_obj_t *, lv_event_t);
static void switch_right_signal(lv_obj_t *, lv_event_t);
static void reset_trip(lv_obj_t *, lv_event_t);
static void go_to_sensor_tab(lv_obj_t *, lv_event_t);
static void go_to_home_tab(lv_obj_t *, lv_event_t);
static void go_to_settings_tab(lv_obj_t *, lv_event_t);
static void change_sensor(lv_obj_t *, lv_event_t);
static void sw_lights(lv_obj_t *, lv_event_t);
static void speed_anim(lv_obj_t *, lv_anim_value_t);
static void signal_left_anim(lv_obj_t *, lv_anim_value_t);
static void signal_right_anim(lv_obj_t *, lv_anim_value_t);
static void batt_anim(lv_obj_t *, lv_anim_value_t);

/*******************************************************************************
 * Code
 ******************************************************************************/
void lv_widgets(void)
{
	lv_style_init(&date_and_time_style);
	lv_style_set_value_align(&date_and_time_style, LV_STATE_DEFAULT, LV_ALIGN_OUT_TOP_LEFT);
	lv_style_set_value_ofs_y(&date_and_time_style, LV_STATE_DEFAULT, - LV_DPX(5));
	lv_style_set_margin_top(&date_and_time_style, LV_STATE_DEFAULT, LV_DPX(40));

	lv_style_init(&bar_style);
	lv_style_set_radius(&bar_style, LV_STATE_DEFAULT, 0);

	lv_obj_t * top_bar = lv_btn_create(lv_scr_act(),NULL);
	lv_obj_set_size(top_bar, SCREEN_WIDTH, TOP_BAR_HEIGHT);
	lv_obj_align(top_bar, NULL, LV_ALIGN_IN_TOP_MID, 0, 0);
	lv_obj_set_style_local_bg_color(top_bar,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,lv_color_make(0,0,0));
	lv_obj_add_style(top_bar, LV_BTN_PART_MAIN,&bar_style);
	lv_obj_set_style_local_border_width(top_bar,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_event_cb(top_bar, go_to_settings_tab);

	date_and_time_label = lv_label_create(top_bar, NULL);

	tab_view = lv_tabview_create(lv_scr_act(), NULL);
	lv_obj_set_pos(tab_view,0,TOP_BAR_HEIGHT);
	lv_obj_set_height(tab_view, SCREEN_HEIGHT-TOP_BAR_HEIGHT);
	lv_tabview_set_btns_pos(tab_view, LV_TABVIEW_TAB_POS_NONE);

	home_tab = lv_tabview_add_tab(tab_view, NULL);

	home_create(home_tab);
}

void create_home_tab(lv_obj_t * parent)
{
	lv_page_set_scrl_layout(parent, LV_LAYOUT_PRETTY_MID);

	lv_obj_t * main_container = lv_cont_create(parent, NULL);
	lv_cont_set_layout(main_container, LV_LAYOUT_PRETTY_MID);
	lv_obj_set_size(main_container, lv_page_get_width_grid(parent, 1, 1), lv_page_get_height_grid(parent, 1, 1));
	lv_obj_set_style_local_pad_top(main_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_pad_bottom(main_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_pad_right(main_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_pad_left(main_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_border_width(main_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_margin_top(main_container, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, -1);

	lv_obj_t * left_container = lv_cont_create(main_container, NULL);
	lv_cont_set_layout(left_container, LV_LAYOUT_PRETTY_TOP);
	lv_obj_set_style_local_border_width(left_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

	LV_FONT_DECLARE(temp_font);
	tempLabelEnable = true;
	temp_label = lv_label_create(left_container,NULL);
	lv_obj_set_style_local_text_font(temp_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&temp_font);
	lv_label_set_text(temp_label ,"---");
	lv_obj_set_style_local_margin_top(temp_label,LV_LABEL_PART_MAIN,LV_STATE_DEFAULT,15);
	lv_obj_set_style_local_pad_left(temp_label,LV_LABEL_PART_MAIN,LV_STATE_DEFAULT,5);

	LV_FONT_DECLARE(turn_signals_50);
	left_turn_signal = lv_btn_create(left_container, NULL);
	lv_obj_set_size(left_turn_signal, 55 , 55);
	lv_obj_set_event_cb(left_turn_signal, switch_left_signal);
	lv_obj_set_style_local_border_width(left_turn_signal,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_margin_top(left_turn_signal,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,35);

	lv_obj_t * left_turn_signal_label = lv_label_create(left_turn_signal, NULL);
	lv_label_set_text(left_turn_signal_label,LV_SYMBOL_LEFT_TURN);
	lv_obj_set_style_local_text_font(left_turn_signal_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&turn_signals_50);

	lv_obj_t * trip = lv_btn_create(left_container, NULL);
	lv_obj_set_style_local_border_width(trip,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_width(trip, 73);
	lv_obj_set_style_local_margin_top(trip,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,25);
	lv_obj_set_event_cb(trip, reset_trip);

	lv_obj_t * trip_label = lv_label_create(trip,NULL);
	lv_label_set_recolor(trip_label, true);
	lv_label_set_text(trip_label ,LV_SYMBOL_REFRESH " #0000ff Trip#\n     180km");

	lv_obj_set_size(left_container, 100 , lv_obj_get_height(main_container));

	LV_IMG_DECLARE(gauge);
	lv_obj_t * gauge_background = lv_img_create(main_container, NULL);
	lv_img_set_src(gauge_background, &gauge);

	lv_obj_t * speed_arc = lv_arc_create(gauge_background, NULL);
	lv_obj_set_size(speed_arc, 245, 245);
	lv_arc_set_bg_angles(speed_arc,0,215);
	lv_arc_set_rotation(speed_arc,127);
	lv_arc_set_range(speed_arc,0,180);
	lv_arc_set_value(speed_arc,0);
	lv_obj_align(speed_arc, NULL, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_local_border_width(speed_arc,LV_ARC_PART_BG,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_line_width(speed_arc,LV_ARC_PART_BG,LV_STATE_DEFAULT,5);
	lv_obj_set_style_local_line_width(speed_arc,LV_ARC_PART_INDIC,LV_STATE_DEFAULT,5);

	LV_FONT_DECLARE(speed_numb_70);
	lv_obj_t * number_label = lv_label_create(speed_arc,NULL);
	lv_obj_set_style_local_text_font(number_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&speed_numb_70);
	lv_label_set_text(number_label ,"0");
	lv_label_set_align(number_label, LV_LABEL_ALIGN_CENTER);
	lv_obj_align(number_label, NULL, LV_ALIGN_CENTER, 0, 0);

	lv_obj_t * batt_arc = lv_arc_create(gauge_background, NULL);
	lv_obj_set_size(batt_arc, 160, 160);
	lv_arc_set_bg_angles(batt_arc,0,130);
	lv_arc_set_rotation(batt_arc,350);
	lv_arc_set_range(batt_arc,0,100);
	lv_arc_set_value(batt_arc,42);
	lv_obj_align(batt_arc, NULL, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_local_border_width(batt_arc,LV_ARC_PART_BG,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_line_width(batt_arc,LV_ARC_PART_BG,LV_STATE_DEFAULT,5);
	lv_obj_set_style_local_line_width(batt_arc,LV_ARC_PART_INDIC,LV_STATE_DEFAULT,5);
	lv_obj_set_style_local_line_color(batt_arc, LV_ARC_PART_INDIC, LV_STATE_DEFAULT, lv_color_make(0, 135, 36));

	LV_FONT_DECLARE(units_20);
	lv_obj_t * unit_label = lv_label_create(gauge_background,NULL);
	lv_obj_set_style_local_text_font(unit_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&units_20);
	lv_label_set_text(unit_label ,"km/h");
	lv_label_set_align(unit_label, LV_LABEL_ALIGN_CENTER);
	lv_obj_align(unit_label, NULL, LV_ALIGN_CENTER, 0, 40);

	lv_obj_t * total_distance_label = lv_label_create(gauge_background,NULL);
	lv_obj_set_style_local_text_font(total_distance_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&units_20);
	lv_label_set_text(total_distance_label ,"000321 km");
	lv_label_set_align(total_distance_label, LV_LABEL_ALIGN_CENTER);
	lv_obj_align(total_distance_label, NULL, LV_ALIGN_CENTER, 0, 85);

	lv_obj_t * batt_container = lv_cont_create(gauge_background, NULL);
	lv_obj_set_style_local_border_width(batt_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_cont_set_layout(batt_container, LV_LAYOUT_PRETTY_MID);
	lv_obj_set_size(batt_container, 30, 50);
	lv_obj_set_style_local_pad_top(batt_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_pad_bottom(batt_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

	LV_IMG_DECLARE(battery_icon);
	lv_obj_t * img_batt = lv_img_create(batt_container, NULL);
	lv_img_set_src(img_batt, &battery_icon);

	batt_label = lv_label_create(batt_container,NULL);
	lv_obj_align(batt_container, NULL, LV_ALIGN_CENTER, 85, 15);

	lv_obj_t * right_container = lv_cont_create(main_container, NULL);
	lv_cont_set_layout(right_container, LV_LAYOUT_PRETTY_TOP);
	lv_obj_set_size(right_container, 100 , lv_obj_get_height(main_container));
	lv_obj_set_style_local_border_width(right_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

	lv_obj_t * lights = lv_btn_create(right_container, NULL);
	lv_obj_set_size(lights, 65 , 55);
	lv_obj_set_style_local_border_width(lights,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_event_cb(lights, sw_lights);

	lv_obj_t * img_lights = lv_img_create(lights, NULL);
	isLightOn ? lv_img_set_src(img_lights, &light_on) : lv_img_set_src(img_lights, &light_off);

	right_turn_signal = lv_btn_create(right_container, NULL);
	lv_obj_set_size(right_turn_signal, 55 , 55);
	lv_obj_set_event_cb(right_turn_signal, switch_right_signal);
	lv_obj_set_style_local_border_width(right_turn_signal,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_margin_top(right_turn_signal,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,20);

	lv_obj_t * right_turn_signal_label = lv_label_create(right_turn_signal, NULL);
	lv_label_set_text(right_turn_signal_label, LV_SYMBOL_RIGHT_TURN);
	lv_obj_set_style_local_text_font(right_turn_signal_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT,&turn_signals_50);

	lv_obj_t * sensor_btn = lv_btn_create(right_container, NULL);
	lv_obj_set_size(sensor_btn, 55 , 55);
	lv_obj_set_style_local_border_width(sensor_btn,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
	lv_obj_set_style_local_margin_top(sensor_btn,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,10);
	lv_obj_set_event_cb(sensor_btn, go_to_sensor_tab);

	LV_IMG_DECLARE(sensor_icon);
	lv_obj_t * img_sensor = lv_img_create(sensor_btn, NULL);
	lv_img_set_src(img_sensor, &sensor_icon);

	lv_anim_init(&signal_left);
	lv_anim_set_var(&signal_left, left_turn_signal_label);
	lv_anim_set_exec_cb(&signal_left, (lv_anim_exec_xcb_t)signal_left_anim);
	lv_anim_set_time(&signal_left , 45000);
	lv_anim_set_repeat_count(&signal_left, LV_ANIM_REPEAT_INFINITE);

	lv_anim_init(&signal_right);
	lv_anim_set_var(&signal_right, right_turn_signal_label);
	lv_anim_set_exec_cb(&signal_right, (lv_anim_exec_xcb_t)signal_right_anim);
	lv_anim_set_time(&signal_right , 45000);
	lv_anim_set_repeat_count(&signal_right, LV_ANIM_REPEAT_INFINITE);

	lv_anim_t batt;
	lv_anim_init(&batt);
	lv_anim_set_var(&batt, batt_arc);
	lv_anim_set_exec_cb(&batt, (lv_anim_exec_xcb_t)batt_anim);
	lv_anim_set_values(&batt, 0, 100);
	lv_anim_set_time(&batt, 20000);
	lv_anim_set_playback_time(&batt, 1000);
	lv_anim_set_repeat_count(&batt, LV_ANIM_REPEAT_INFINITE);
	lv_anim_start(&batt);

	lv_anim_t speed;
	lv_anim_init(&speed);
	lv_anim_set_var(&speed, speed_arc);
	lv_anim_set_exec_cb(&speed, (lv_anim_exec_xcb_t)speed_anim);
	lv_anim_set_values(&speed, 0, 180);
	lv_anim_set_time(&speed, 20000);
	lv_anim_set_playback_time(&speed, 1000);
	lv_anim_set_repeat_count(&speed, LV_ANIM_REPEAT_INFINITE);
	lv_anim_start(&speed);
}

static void home_create(lv_obj_t * parent)
{
	create_home_tab(parent);
}

static void set_date_and_time(lv_obj_t * button, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_calendar_date_t * current_date = lv_calendar_get_today_date(calendar);
		date.year   = current_date->year;
		date.month  = current_date->month;
		date.day    = current_date->day;

		date.hour = lv_roller_get_selected(hours_roller);
		date.minute = lv_roller_get_selected(minutes_roller);
		date.second = 0;

		RTC_EnableTimer(RTC, false);
		RTC_SetDatetime(RTC, &date);
		RTC_EnableTimer(RTC, true);

		lv_tabview_clean_tab(home_tab);
		create_home_tab(home_tab);
	}
}

static void check_date(lv_obj_t * calendar, lv_event_t event)
{
	if(event == LV_EVENT_VALUE_CHANGED)
	{
		lv_calendar_date_t * date = lv_calendar_get_pressed_date(calendar);
		if(date)
		{
			lv_calendar_set_today_date(calendar, date);
			lv_calendar_set_showed_date(calendar, date);
		}
	}
}

static void switch_left_signal(lv_obj_t * button, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_obj_t * left_turn_label = lv_obj_get_child(button, NULL);
		if(isRightEnable)
		{
			lv_obj_t * right_turn_label = lv_obj_get_child(right_turn_signal, NULL);
			lv_obj_set_style_local_text_color(right_turn_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
			isRightEnable = false;
			lv_anim_del(right_turn_label, (lv_anim_exec_xcb_t) signal_right_anim);
		}

		if(!isLeftEnable)
		{
			isLeftEnable = true;
			lv_anim_start(&signal_left);
		}
		else
		{
			lv_obj_set_style_local_text_color(left_turn_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
			isLeftEnable = false;
			lv_anim_del(left_turn_label, (lv_anim_exec_xcb_t) signal_left_anim);
		}
	}
}

static void switch_right_signal(lv_obj_t * button, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_obj_t * right_turn_label = lv_obj_get_child(button, NULL);
		if(isLeftEnable)
		{
			lv_obj_t * left_turn_label = lv_obj_get_child(left_turn_signal, NULL);
			lv_obj_set_style_local_text_color(left_turn_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
			isLeftEnable = false;
			lv_anim_del(left_turn_label, (lv_anim_exec_xcb_t) signal_left_anim);
		}

		if(!isRightEnable)
		{
			isRightEnable = true;
			lv_anim_start(&signal_right);
		}
		else
		{
			lv_obj_set_style_local_text_color(right_turn_label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
			isRightEnable = false;
			lv_anim_del(right_turn_label, (lv_anim_exec_xcb_t) signal_right_anim);
		}
	}
}

static void reset_trip(lv_obj_t * button, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_obj_t * label = lv_obj_get_child(button, NULL);
		lv_label_set_text(label ,LV_SYMBOL_REFRESH " #0000ff Trip#\n     0 km");
	}
}

static void go_to_sensor_tab(lv_obj_t * button, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_tabview_clean_tab(home_tab);

		tempLabelEnable = false;
		tofSensorEnable = true;

		tof_sensor.platform.address = 0x20;
		if(frontSensorConnected) vl53l5cx_start_ranging(&tof_sensor);

		lv_obj_t * control_container = lv_cont_create(home_tab, NULL);
		lv_cont_set_layout(control_container, LV_LAYOUT_PRETTY_MID);
		lv_cont_set_fit2(control_container, LV_FIT_NONE, LV_FIT_TIGHT);
		lv_obj_set_width(control_container, lv_page_get_width_grid(home_tab, 1, 1));
		lv_obj_set_style_local_border_width(control_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

		lv_obj_t * previous_button = lv_btn_create(control_container, NULL);
		lv_obj_set_style_local_bg_color(previous_button, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, lv_color_make(0,0,0));
		lv_obj_set_style_local_border_width(previous_button,LV_BTN_PART_MAIN,LV_STATE_DEFAULT,0);
		lv_obj_set_width(previous_button, 60);
		lv_obj_set_event_cb(previous_button, go_to_home_tab);

		lv_obj_t * previous_button_label = lv_label_create(previous_button, NULL);
		lv_label_set_text(previous_button_label, LV_SYMBOL_LEFT);

		sw_sensor = lv_switch_create(control_container, NULL);
		lv_obj_set_style_local_value_str(sw_sensor, LV_SWITCH_PART_BG, LV_STATE_DEFAULT, "Front");
		lv_obj_set_style_local_value_align(sw_sensor, LV_SWITCH_PART_BG, LV_STATE_DEFAULT, LV_ALIGN_OUT_LEFT_MID);
		lv_obj_set_style_local_value_ofs_x(sw_sensor, LV_SWITCH_PART_BG, LV_STATE_DEFAULT, -5);
		lv_obj_set_width(sw_sensor,44);
		lv_obj_set_event_cb(sw_sensor, change_sensor);

		img_from_sensor = lv_img_create(home_tab, NULL);
		lv_img_set_src(img_from_sensor, &my_img_from_sensor);
		lv_obj_align(img_from_sensor, NULL, LV_ALIGN_CENTER, 0, 0);
		lv_obj_set_style_local_margin_top(img_from_sensor, LV_IMG_PART_MAIN, LV_STATE_DEFAULT, -4);
	}
}

static void go_to_home_tab(lv_obj_t * btn, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_tabview_clean_tab(home_tab);
		tofSensorEnable = false;
		create_home_tab(home_tab);
		if((frontSensorConnected && !lv_switch_get_state(sw_sensor)) || (backSensorConnected && lv_switch_get_state(sw_sensor)))
			vl53l5cx_stop_ranging(&tof_sensor);
	}
}

static void go_to_settings_tab(lv_obj_t * btn, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		tempLabelEnable = false;
		tofSensorEnable = false;

		lv_tabview_clean_tab(home_tab);
		lv_page_set_scrl_layout(home_tab, LV_LAYOUT_PRETTY_MID);

		calendar = lv_calendar_create(home_tab, NULL);
		lv_obj_set_size(calendar, lv_page_get_width_grid(home_tab, 2, 1), 170);
		lv_obj_set_event_cb(calendar, check_date);
		lv_obj_set_style_local_value_str(calendar, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Date");
		lv_obj_add_style(calendar, LV_CONT_PART_MAIN, &date_and_time_style);
		lv_obj_set_style_local_border_width(calendar,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

		lv_calendar_date_t current_date;
		current_date.year = date.year;
		current_date.month = date.month;
		current_date.day = date.day;

		lv_calendar_set_today_date(calendar, &current_date);
		lv_calendar_set_showed_date(calendar, &current_date);

		lv_obj_t * time_container = lv_cont_create(home_tab, NULL);
		lv_cont_set_layout(time_container, LV_LAYOUT_PRETTY_MID);
		lv_obj_add_style(time_container, LV_CONT_PART_MAIN, &date_and_time_style);
		lv_obj_set_size(time_container, lv_page_get_width_grid(home_tab, 2, 1), lv_obj_get_height(calendar));
		lv_obj_set_style_local_value_str(time_container, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Time");
		lv_obj_set_style_local_border_width(time_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

		hours_roller = lv_roller_create(time_container, NULL);
		lv_obj_add_style(hours_roller, LV_CONT_PART_MAIN, &date_and_time_style);
		lv_obj_set_style_local_value_str(hours_roller, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Hours");
		lv_roller_set_auto_fit(hours_roller, false);
		lv_roller_set_align(hours_roller, LV_LABEL_ALIGN_CENTER);
		lv_roller_set_options(hours_roller,"00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23",LV_ROLLER_MODE_INFINITE);
		lv_roller_set_selected(hours_roller,date.hour,LV_ANIM_ON);
		lv_roller_set_visible_row_count(hours_roller, 4);

		minutes_roller = lv_roller_create(time_container, NULL);
		lv_obj_add_style(minutes_roller, LV_CONT_PART_MAIN, &date_and_time_style);
		lv_obj_set_style_local_value_str(minutes_roller, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, "Minutes");
		lv_roller_set_auto_fit(minutes_roller, false);
		lv_roller_set_align(minutes_roller, LV_LABEL_ALIGN_CENTER);
		lv_roller_set_options(minutes_roller,"00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n27\n28\n"
				"29\n30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n50\n51\n52\n53\n54\n55\n56\n57\n58\n59",
				LV_ROLLER_MODE_INFINITE);
		lv_roller_set_selected(minutes_roller,date.minute,LV_ANIM_OFF);
		lv_roller_set_visible_row_count(minutes_roller, 4);

		lv_obj_t * button_container = lv_cont_create(home_tab, NULL);
		lv_cont_set_layout(button_container, LV_LAYOUT_PRETTY_MID);
		lv_obj_set_style_local_margin_top(button_container, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, -15);
		lv_obj_set_style_local_pad_top(button_container, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 5);
		lv_obj_set_style_local_pad_bottom(button_container, LV_CONT_PART_MAIN, LV_STATE_DEFAULT, 5);
		lv_cont_set_fit2(button_container, LV_FIT_NONE, LV_FIT_TIGHT);
		lv_obj_set_width(button_container, lv_page_get_width_grid(home_tab, 1, 1));
		lv_obj_set_style_local_border_width(button_container,LV_CONT_PART_MAIN,LV_STATE_DEFAULT,0);

		lv_obj_t * apply_button = lv_btn_create(button_container, NULL);
		lv_obj_set_style_local_bg_color(apply_button, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, lv_theme_get_color_primary());
		lv_obj_set_event_cb(apply_button, set_date_and_time);

		lv_obj_t * apply_button_label = lv_label_create(apply_button, NULL);
		lv_label_set_text(apply_button_label, "Apply");
	}
}

static void change_sensor(lv_obj_t * sw, lv_event_t event)
{
	if(event == LV_EVENT_VALUE_CHANGED)
	{
		if(lv_switch_get_state(sw))
		{
			lv_obj_set_style_local_value_str(sw, LV_SWITCH_PART_BG, LV_STATE_DEFAULT, "Back");
			if(frontSensorConnected) vl53l5cx_stop_ranging(&tof_sensor);
			if(backSensorConnected)
			{
				tof_sensor.platform.address = 0x29;
				vl53l5cx_start_ranging(&tof_sensor);
			}
		}
		else
		{
			lv_obj_set_style_local_value_str(sw, LV_SWITCH_PART_BG, LV_STATE_DEFAULT, "Front");
			if(backSensorConnected) vl53l5cx_stop_ranging(&tof_sensor);
			if(frontSensorConnected)
			{
				tof_sensor.platform.address = 0x20;
				vl53l5cx_start_ranging(&tof_sensor);
			}
		}
	}
}

static void sw_lights(lv_obj_t * btn, lv_event_t event)
{
	if(event == LV_EVENT_CLICKED)
	{
		lv_obj_t * image = lv_obj_get_child(btn, NULL);
		isLightOn ? lv_img_set_src(image, &light_off) : lv_img_set_src(image, &light_on);
		isLightOn = !isLightOn;
	}
}

static void speed_anim(lv_obj_t * arc, lv_anim_value_t value)
{
	lv_arc_set_value(arc, value);

	static float float_val;

	if(value <= 90)
	{
		speed = value-(value/3);
		float_val = speed;
	}
	else
	{
		float_val+=1.34;
		speed = float_val;
	}

	static char buf[5];
	lv_snprintf(buf, sizeof(buf), "%d", speed);

	lv_obj_t * label = lv_obj_get_child(arc, NULL);
	lv_label_set_text(label, buf);
	lv_obj_align(label, NULL, LV_ALIGN_CENTER, 0, 0);
}

static void signal_left_anim(lv_obj_t * label, lv_anim_value_t value)
{
	static bool signal_flag = false;
	signal_flag ? lv_obj_set_style_local_text_color(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, lv_color_make(0, 135, 36))
			: lv_obj_set_style_local_text_color(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
	signal_flag = !signal_flag;
}

static void signal_right_anim(lv_obj_t * label, lv_anim_value_t value)
{
	static bool signal_flag = false;
	signal_flag ? lv_obj_set_style_local_text_color(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, lv_color_make(0, 135, 36))
			: lv_obj_set_style_local_text_color(label, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
	signal_flag = !signal_flag;
}

static void batt_anim(lv_obj_t * arc, lv_anim_value_t value)
{
	lv_arc_set_value(arc, value);

	static char buf[5];
	lv_snprintf(buf, sizeof(buf), "%d%%", value);

	lv_label_set_text(batt_label, buf);
	lv_obj_align(batt_label, NULL, LV_ALIGN_CENTER, 0, 0);
}
