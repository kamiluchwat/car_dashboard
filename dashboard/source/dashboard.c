#include <lv_widgets/src/lv_widgets.h>
#include "fsl_debug_console.h"
#include "littlevgl_support.h"
#include "pin_mux.h"
#include "board.h"
#include "lvgl.h"
#include "fsl_rtc.h"
#include "peripherals.h"
#include "arm_math.h"

#include <mcp9808/mcp9808.h>
#include <vl53l5cx/vl53l5cx_api.h>
#include <vl53l5cx/platform.h>
#include "colors.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#ifndef LVGL_TICK_MS
#define LVGL_TICK_MS 1U
#endif

#ifndef LVGL_TASK_PERIOD_TICK
#define LVGL_TASK_PERIOD_TICK 5U
#endif

#define SOURCE_SIZE 64
#define INTERP_SIZE 176
#define RATIO 0.045f

/*******************************************************************************
 * Variables
 ******************************************************************************/
static volatile uint32_t s_tick        = 0U;
static volatile bool s_lvglTaskPending = false;

static mcp9808_device temp_sensor;
static char buff[25];
static char month[10];
static int8_t temperature;
static bool tempSensorEnable;

static uint8_t status, p_data_ready, resolution, isAlive, rgb;
static VL53L5CX_ResultsData results;
static float colors_8x8[SOURCE_SIZE] = {0};
static arm_bilinear_interp_instance_f32 S = {8,8,colors_8x8};

extern lv_obj_t * date_and_time_label;
extern lv_obj_t * temp_label;
extern bool tempLabelEnable;
extern bool tofSensorEnable;
extern lv_obj_t * img_from_sensor;
extern lv_img_dsc_t my_img_from_sensor;
extern uint8_t speed;

VL53L5CX_Configuration tof_sensor;
rtc_datetime_t date;
uint8_t colors_176x176[61952] = {0};
bool frontSensorConnected = false, backSensorConnected = false;
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void SetupTick(void);

/*******************************************************************************
 * Code
 ******************************************************************************/
void get_data_by_polling(VL53L5CX_Configuration *p_dev)
{
	status = vl53l5cx_check_data_ready(p_dev, &p_data_ready);

	if(p_data_ready)
	{
		uint16_t counter = 0, interpolated_value = 0;
		float x = 0, y = 0, max = 0, min = 4000;

		status = vl53l5cx_get_ranging_data(p_dev, &results);

		for(int i = 0 ; i < SOURCE_SIZE ; i++)
		{
			if(max < results.distance_mm[VL53L5CX_NB_TARGET_PER_ZONE * i]) max = results.distance_mm[VL53L5CX_NB_TARGET_PER_ZONE * i];
			if(min > results.distance_mm[VL53L5CX_NB_TARGET_PER_ZONE * i]) min = results.distance_mm[VL53L5CX_NB_TARGET_PER_ZONE * i];
		}

		for(int i = 0; i < SOURCE_SIZE ; i++)
		{
			rgb = (1.0f - ((results.distance_mm[(VL53L5CX_NB_TARGET_PER_ZONE * i)] - min)/(max-min))) * 255.0f;
			colors_8x8[i] = rgb;
		}

		for(int j=0;j<INTERP_SIZE;j++){
			y = j*RATIO;
			if(y < 1) y = 1;
			for(int i=0;i<INTERP_SIZE;i++){
				x = i*RATIO;
				if(x < 1) x = 1;
				interpolated_value = arm_bilinear_interp_f32(&S,x ,y);
				interpolated_value = colors_256x1[interpolated_value];
				colors_176x176[counter++] = interpolated_value & 0xff;
				colors_176x176[counter++] = (interpolated_value >> 8);
			}
		}
		lv_img_set_src(img_from_sensor, &my_img_from_sensor);
	}
}

int main(void)
{
	CLOCK_AttachClk(BOARD_DEBUG_UART_CLK_ATTACH);

	SYSCON->RTCOSCCTRL |= SYSCON_RTCOSCCTRL_EN_MASK;

	BOARD_InitPins();
	BOARD_BootClockPLL220M();
	BOARD_InitDebugConsole();
	BOARD_InitSDRAM();

	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();

	SetupTick();

	lv_port_pre_init();
	lv_init();
	lv_port_disp_init();
	lv_port_indev_init();

	tof_sensor.platform.i2c = ((I2C_Type *)I2C2_BASE);
	tof_sensor.platform.address = 0x29;

	status = vl53l5cx_set_i2c_address(&tof_sensor, 0x20<<1);
	tof_sensor.platform.address = 0x20;

	status = vl53l5cx_is_alive(&tof_sensor, &isAlive);
	if(!isAlive && status==0)
	{
		PRINTF("VL53L5CXV0 not detected at requested address (0x%x)\r\n", tof_sensor.platform.address);
	}
	else
	{
		PRINTF("VL53L5CXV0 is Alive at address (0x%x)\r\n", tof_sensor.platform.address);

		PRINTF("Sensor initializing, please wait few seconds...\r\n");
		status = vl53l5cx_init(&tof_sensor);
		status = vl53l5cx_set_resolution(&tof_sensor, VL53L5CX_RESOLUTION_8X8);
		status = vl53l5cx_set_ranging_frequency_hz(&tof_sensor, 15);
		status = vl53l5cx_set_ranging_mode(&tof_sensor, VL53L5CX_RANGING_MODE_CONTINUOUS);

		frontSensorConnected = true;
	}

	tof_sensor.platform.address = 0x29;

	GPIO_PinWrite(BOARD_INITPINS_TLPn_2_GPIO, BOARD_INITPINS_TLPn_2_PORT, BOARD_INITPINS_TLPn_2_PIN, 1);

	status = vl53l5cx_is_alive(&tof_sensor, &isAlive);
	if(!isAlive && status==0)
	{
		PRINTF("VL53L5CXV0 not detected at requested address (0x%x)\r\n", tof_sensor.platform.address);
	}
	else
	{
		PRINTF("VL53L5CXV0 is Alive at address (0x%x)\r\n", tof_sensor.platform.address);

		PRINTF("Sensor initializing, please wait few seconds...\r\n");
		status = vl53l5cx_init(&tof_sensor);
		status = vl53l5cx_set_resolution(&tof_sensor, VL53L5CX_RESOLUTION_8X8);
		status = vl53l5cx_set_ranging_frequency_hz(&tof_sensor, 15);
		status = vl53l5cx_set_ranging_mode(&tof_sensor, VL53L5CX_RANGING_MODE_CONTINUOUS);

		backSensorConnected = true;
	}

	tof_sensor.platform.address = 0x20;

	RTC_Init(RTC);

	date.year   = 2022;
	date.month  = 1;
	date.day    = 1;
	date.hour   = 0;
	date.minute = 0;
	date.second = 0;

	RTC_EnableTimer(RTC, false);
	RTC_SetDatetime(RTC, &date);
	RTC_EnableTimer(RTC, true);

	lv_widgets();

	temp_sensor.i2c = FLEXCOMM1_PERIPHERAL;
	temp_sensor.address = MCP9808_I2CADDR_DEFAULT;

	if(tempLabelEnable)
		tempSensorEnable = mcp9808_init(&temp_sensor);

	for (;;)
	{
		USART_WriteBlocking(FLEXCOMM4_PERIPHERAL, &speed, 1);

		if(tempLabelEnable & tempSensorEnable)
		{
			temperature = read_temp_C(&temp_sensor);

			lv_snprintf(buff, sizeof(buff), "%d°C",temperature);
			lv_label_set_text(temp_label, buff);
		}

		if(tofSensorEnable && (frontSensorConnected || backSensorConnected))
			get_data_by_polling(&tof_sensor);

		RTC_GetDatetime(RTC, &date);

		switch(date.month)
		{
			case 1: { strcpy(month,"January");   } break;
			case 2: { strcpy(month,"February");  } break;
			case 3: { strcpy(month,"March");     } break;
			case 4: { strcpy(month,"April");     } break;
			case 5: { strcpy(month,"May");       } break;
			case 6: { strcpy(month,"June");      } break;
			case 7: { strcpy(month,"July");      } break;
			case 8: { strcpy(month,"August");    } break;
			case 9: { strcpy(month,"September"); } break;
			case 10:{ strcpy(month,"October");   } break;
			case 11:{ strcpy(month,"November");  } break;
			case 12:{ strcpy(month,"December");  } break;
		}

		lv_snprintf(buff, sizeof(buff), "%02d %s %04d  %02d:%02d",date.day, month, date.year, date.hour,date.minute);
		lv_label_set_text(date_and_time_label, buff);

		while (!s_lvglTaskPending);
		s_lvglTaskPending = false;

		lv_task_handler();
	}
}

static void SetupTick(void)
{
	if (0 != SysTick_Config(SystemCoreClock / (LVGL_TICK_MS * 1000U)))
	{
		PRINTF("Tick initialization failed\r\n");
		while (1);
	}
}

void SysTick_Handler(void)
{
	s_tick++;
	lv_tick_inc(LVGL_TICK_MS);

	if ((s_tick % LVGL_TASK_PERIOD_TICK) == 0U)
		s_lvglTaskPending = true;
}
