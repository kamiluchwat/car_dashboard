#include "fsl_i2c.h"
#include "mcp9808.h"

static uint16_t read_register16(mcp9808_device *dev, uint8_t addr)
{
	uint8_t rx_buff[2];

	if (kStatus_Success == I2C_MasterStart(dev->i2c, dev->address, kI2C_Write))
		I2C_MasterWriteBlocking(dev->i2c, &addr, 1, kI2C_TransferDefaultFlag);

	if (kStatus_Success == I2C_MasterStart(dev->i2c, dev->address, kI2C_Read))
		I2C_MasterReadBlocking(dev->i2c, rx_buff, 2, kI2C_TransferDefaultFlag);

	uint16_t retVal = (uint16_t) ((rx_buff[0] << 8) | rx_buff[1]);

	return retVal;
}


static int write_register16(mcp9808_device *dev, uint8_t addr, uint16_t value)
{
	uint8_t tx_buff[3]={addr, value >> 8, value & 0xFF};

	if (kStatus_Success == I2C_MasterStart(dev->i2c, dev->address, kI2C_Write))
		I2C_MasterWriteBlocking(dev->i2c, tx_buff, 3, kI2C_TransferDefaultFlag);
	else
		return 1;

	return 0;
}

static void shutdown(mcp9808_device *device, bool sw_id)
{
    uint16_t conf_register = read_register16(device, MCP9808_REG_CONFIG);

    if(sw_id)
    {
    	uint16_t conf_shutdown = conf_register | MCP9808_REG_CONFIG_SHUTDOWN;
    	write_register16(device,MCP9808_REG_CONFIG,conf_shutdown);

    }
    else
    {
    	uint16_t conf_shutdown = conf_register & (!MCP9808_REG_CONFIG_SHUTDOWN);
    	write_register16(device,MCP9808_REG_CONFIG,conf_shutdown);
    }
}

bool mcp9808_init(mcp9808_device *device)
{
	if(read_register16(device, MCP9808_REG_MANUF_ID) != 0x0054) return false;
	if(read_register16(device, MCP9808_REG_DEVICE_ID) != 0x0400) return false;

	shutdown(device,false);

	return true;
}

float read_temp_C(mcp9808_device *device)
{
	uint16_t t = read_register16(device, MCP9808_REG_AMBIENT_TEMP);
	float temp = t & 0x0FFF;
	temp /= 16.0;
	if(t & 0x1000) temp -= 256;

	return temp;
}
