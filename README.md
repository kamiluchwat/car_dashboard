Development Board: NXP LPCXpresso54628 <br />
Temperature sensor: Adafruit MCP9808 (I2C) <br />
Time-of-Flight sensors: STM VL53L5CX (I2C) <br />
Libraries: LVGL, CMSIS (bilinear interpolation) <br />


Main Screen
![Scheme](images/main_screen.jpg)

Settings Screen
![Scheme](images/settings_screen.jpg)

Lidar Screen
![Scheme](images/lidar_screen.jpg)
